# README #

This is a tech test for tray.io based on their Figma.

## Caveats

* The mockup is made with regular (outlined) FontAwesome icons, which I don't have access to, so I'm using
  free (solid) icons instead. With access to a Pro account, substituting the icons for the proper ones from
  the Figma mockup is a simple matter of replacing `@fortawesome/free-solid-svg-icons` with a properly
  configured `@fortawesome/pro-regular-svg-icons`. For details, see the FontAwesome React
  [documentation](https://github.com/FortAwesome/react-fontawesome).
* I spent quite a lot of my time on setup and "architecture", so there aren't that many components! I hope
  what I've provided gives a good impression of how I would go about writing components and composing them.

## Next steps

* If I had more time the next set of components I would handle would be the item lists (the list of workflows
  and authentications). I've already got most of the typography and icons set up for it.
* For the left navigation, a next step would be to add a component that switches it to a burger menu on a
  small screen. The `<nav>` would also be made scrollable so that the slide in (on small screens) or the 
  grid (on big screens) can be locked to the height of the viewport and the nav and main sections scroll
  independently of each other. The avatar would go in as an atom, and added to the top of the nav list,
  presumably linking to a user settings screen.
* More cross-browser testing! (The nav spacer doesn't work in Firefox)
* The navigation handling function in NavButton and NavMenuItem can be made into a HOC.

## Setting up

The usual deal. Just run:

```bash
npm ci
```

## Running the style guide

```bash
npm run styleguide
```

## Building the style guide

To build the styleguide statically, run:

```bash
npm run styleguide:build
```

This will build a bunch of static HTML files in the `storybook-static/` directory.

## Using the components

For the purposes of this exercise I haven't exported anything. However in a real-world
scenario there would be a bunch of exports in a file, then you would be able to include
it in a project by running:

```bash
npm install git+ssh://git@bitbucket.org/peter_hall/trayio-storybook.git
```
