import React from 'react';
import { storiesOf } from '@storybook/react';
import PropTypes from 'prop-types';

import {
  primaryBackgroundColourCss,
  secondaryBackgroundColourCss,
  textColourCss,
  primaryTextColourCss,
  secondaryTextColourCss,
  highlightedTextColourCss,
  mutedTextColourCss,
  inverseTextColourCss,
  fillColourCss,
  primaryFillColourCss,
  secondaryFillColourCss,
  borderColourCss,
} from './Colours';
import { hoverFillColourCss } from './Colours.style';

const Swatch = ({ cssObj, name }) => (
  <div style={{
    alignContent: 'center',
    display: 'flex',
    flexDirection: 'row',
    marginBottom: '1rem',
  }}
  >
    <div style={{
      backgroundColor: '#FFCC88',
      padding: '5px',
      marginRight: '1em',
    }}
    >
      <div
        css={[cssObj]}
        style={{
          alignContent: 'fill',
          display: 'flex',
          flexDirection: 'column',
          fontSize: '2.5rem',
          justifyContent: 'center',
          height: '5rem',
          width: '5rem',
        }}
      >
        <div style={{ textAlign: 'center' }}>abc</div>
      </div>
    </div>
    <p style={{
      margin: 0,
      padding: 0,
    }}
    >
      {name}
    </p>
  </div>
);

Swatch.propTypes = {
  name: PropTypes.string.isRequired,
  cssObj: PropTypes.shape({
    name: PropTypes.string,
    styles: PropTypes.string,
  }).isRequired,
};

storiesOf('Particles/Colours', module)
  .add(
    'Backgrounds', () => (
      <>
        <Swatch cssObj={primaryBackgroundColourCss} name="Primary Background" />
        <Swatch cssObj={secondaryBackgroundColourCss} name="Secondary Background" />
      </>
    ),
  )
  .add(
    'Text', () => (
      <>
        <Swatch cssObj={textColourCss} name="Text" />
        <Swatch cssObj={primaryTextColourCss} name="Primary text" />
        <Swatch cssObj={secondaryTextColourCss} name="Secondary text" />
        <Swatch cssObj={highlightedTextColourCss} name="Highlighted text" />
        <Swatch cssObj={mutedTextColourCss} name="Muted text" />
        <Swatch cssObj={inverseTextColourCss} name="Inverted text" />
      </>
    ),
  )
  .add(
    'Fills', () => (
      <>
        <Swatch cssObj={fillColourCss} name="Fill" />
        <Swatch cssObj={primaryFillColourCss} name="Primary fill" />
        <Swatch cssObj={secondaryFillColourCss} name="Secondary fill" />
        <Swatch cssObj={hoverFillColourCss} name="Hover fill" />
      </>
    ),
  )
  .add(
    'Borders', () => (
      <>
        <Swatch cssObj={borderColourCss} name="Section Border" />
      </>
    ),
  );
