import { css } from '@emotion/core';

// Backgrounds
export const COLOUR_BACKGROUND_PRIMARY = '#FFFFFF';
export const COLOUR_BACKGROUND_SECONDARY = '#E5E5E5';

// Texts
export const COLOUR_TEXT = '#5C5C70';
export const COLOUR_TEXT_PRIMARY = '#0A0D36';
export const COLOUR_TEXT_SECONDARY = '#1A1A3B';
export const COLOUR_TEXT_HIGHLIGHT = '#0D66E5';
export const COLOUR_TEXT_MUTED = '#878796';
export const COLOUR_TEXT_INVERSE = '#FFFFFF';


// Fills
export const COLOUR_FILL = '#B8B8C7';
export const COLOUR_FILL_PRIMARY = '#0D66E5';
export const COLOUR_FILL_SECONDARY = '#5CA1F7';
export const COLOUR_FILL_HOVER = '#F6F6F6';

// Borders
export const COLOUR_BORDER = '#E7E7EB';

// Backgrounds
export const primaryBackgroundColourCss = css`
  background-color: ${COLOUR_BACKGROUND_PRIMARY};
`;
export const secondaryBackgroundColourCss = css`
  background-color: ${COLOUR_BACKGROUND_SECONDARY};
`;

// Texts
export const textColourCss = css`
  color: ${COLOUR_TEXT};
`;
export const primaryTextColourCss = css`
  color: ${COLOUR_TEXT_PRIMARY};
`;
export const secondaryTextColourCss = css`
  color: ${COLOUR_TEXT_SECONDARY};
`;
export const highlightedTextColourCss = css`
  color: ${COLOUR_TEXT_HIGHLIGHT};
`;
export const mutedTextColourCss = css`
  color: ${COLOUR_TEXT_MUTED};
`;
export const inverseTextColourCss = css`
  color: ${COLOUR_TEXT_INVERSE};
`;

// Fills
export const fillColourCss = css`
  background-color: ${COLOUR_FILL};
`;
export const primaryFillColourCss = css`
  background-color: ${COLOUR_FILL_PRIMARY};
`;
export const secondaryFillColourCss = css`
  background-color: ${COLOUR_FILL_SECONDARY};
`;
export const hoverFillColourCss = css`
  background-color: ${COLOUR_FILL_HOVER};
`;

// Borders
export const borderColourCss = css`
  border: 1px solid ${COLOUR_BORDER};
`;
