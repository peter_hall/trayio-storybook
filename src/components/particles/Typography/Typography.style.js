import { css } from '@emotion/core';

export const primaryHeadingTypographyCss = css`
  font-family: 'Nunito Sans', sans-serif;
  font-size: ${22 / 16}rem;
  font-weight: 600;
`;

export const secondaryHeadingTypographyCss = css`
  font-family: 'Nunito Sans', sans-serif;
  font-size: ${18 / 16}rem;
  font-weight: 600;
`;

export const normalTypographyCss = css`
  font-family: 'Nunito Sans', sans-serif;
  font-size: 1rem;
  font-weight: 300;
  text-decoration: none;
`;

export const inputLabelTypographyCss = css`
  font-family: 'Nunito Sans', sans-serif;
  font-size: ${12 / 16}rem;
  font-weight: 600;
`;

export const inputTypographyCss = css`
  font-family: 'Nunito Sans', sans-serif;
  font-size: ${14 / 16}rem;
  font-weight: 600;
`;

export const mutedTypographyCss = css`
  font-family: 'Nunito Sans', sans-serif;
  font-size: ${14 / 16}rem;
  font-weight: 300;
`;

export const activeTypographyCss = css`
  font-weight: 600;
`;
