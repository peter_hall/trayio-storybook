import React from 'react';
import { storiesOf } from '@storybook/react';

import {
  primaryHeadingTypographyCss,
  secondaryHeadingTypographyCss,
  normalTypographyCss,
  inputLabelTypographyCss,
  inputTypographyCss,
  mutedTypographyCss,
  activeTypographyCss,
} from './Typography.style';

storiesOf('Particles/Typography', module)
  .add(
    'default',
    () => (
      <>
        <p css={primaryHeadingTypographyCss}>Primary headings</p>
        <p css={[secondaryHeadingTypographyCss]}>Secondary headings</p>
        <p css={[normalTypographyCss]}>Normal text</p>
        <p css={[inputLabelTypographyCss]}>Input label text</p>
        <p css={[inputTypographyCss]}>Input text</p>
        <p css={[mutedTypographyCss]}>Muted text</p>
        <p css={[normalTypographyCss, activeTypographyCss]}>Active text</p>
      </>
    ),
  );
