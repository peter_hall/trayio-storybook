import React from 'react';
import { storiesOf } from '@storybook/react';
import PropTypes from 'prop-types';

import {
  hoverBorderStyleCss,
  sectionBorderStyleCss,
} from './Borders';

const Swatch = ({ cssObj, name }) => (
  <div style={{
    alignContent: 'center',
    display: 'flex',
    flexDirection: 'row',
    marginBottom: '1rem',
  }}
  >
    <div
      css={[cssObj]}
      style={{
        backgroundColor: '#CCCCCC',
        borderColor: '#888888',
        height: '5rem',
        marginRight: '1em',
        width: '5rem',
      }}
    />
    <p style={{
      margin: 0,
      padding: 0,
    }}
    >
      {name}
    </p>
  </div>
);

Swatch.propTypes = {
  name: PropTypes.string.isRequired,
  cssObj: PropTypes.shape({
    name: PropTypes.string,
    styles: PropTypes.string,
  }).isRequired,
};

storiesOf('Particles/Borders', module)
  .add('default', () => (
    <>
      <Swatch cssObj={hoverBorderStyleCss} name="Hover border style" />
      <Swatch cssObj={sectionBorderStyleCss} name="Section border style" />
    </>
  ));
