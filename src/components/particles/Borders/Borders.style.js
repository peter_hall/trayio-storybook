import { css } from '@emotion/core';

export const sectionBorderStyleCss = css`
  border-width: 1px;
  border-style: solid;
`;

export const hoverBorderStyleCss = css`
  border-radius: 0.5rem;
  border-width: 0;
`;
