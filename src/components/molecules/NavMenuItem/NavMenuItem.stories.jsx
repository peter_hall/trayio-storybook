import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';

import NavMenuItem from './NavMenuItem';

storiesOf('Molecules/NavMenuItem', module)
  .addDecorator(withKnobs)
  .addDecorator((story) => <ul style={{ margin: 0, padding: 0 }}>{story()}</ul>)
  .add('default', () => (
    <NavMenuItem
      href={text('href', '/dashboard')}
      icon={text('icon', 'home')}
      onNavigate={action('onNavigate')}
    >
      {text('Text', 'Dashboard')}
    </NavMenuItem>
  ));
