import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import NavMenuItem from './NavMenuItem';

describe('<NavMenuItem/>', () => {
  describe('@render', () => {
    it('should contain the supplied text', () => {
      const { getByText } = render(<NavMenuItem href="/">Brontosaurus</NavMenuItem>);
      expect(getByText('Brontosaurus')).toBeTruthy();
    });
    it('should render as active if isActive is true', () => {
      const { queryAllByText } = render(<NavMenuItem href="/" isActive>Brontosaurus</NavMenuItem>);
      const [link] = queryAllByText((_, element) => (
        element.textContent === 'Brontosaurus' && element.tagName.toLowerCase() === 'a'
      ));
      expect(link).toHaveAttribute('data-active', 'true');
    });
    it('should render inactive by default', () => {
      const { queryAllByText } = render(<NavMenuItem href="/">Brontosaurus</NavMenuItem>);
      const [link] = queryAllByText((_, element) => (
        element.textContent === 'Brontosaurus' && element.tagName.toLowerCase() === 'a'
      ));
      expect(link).toHaveAttribute('data-active', 'false');
    });
  });
  describe('@click', () => {
    it('should trigger the onNavigation prop', () => {
      const onNavigate = jest.fn();
      const { queryAllByText } = render(
        <NavMenuItem href="!" onNavigate={onNavigate}>Brontosaurus</NavMenuItem>,
      );
      const [link] = queryAllByText((_, element) => (
        element.textContent === 'Brontosaurus' && element.tagName.toLowerCase() === 'a'
      ));
      expect(onNavigate).toHaveBeenCalledTimes(0);
      fireEvent.click(link);
      expect(onNavigate).toHaveBeenCalledWith('!');
      expect(onNavigate).toHaveBeenCalledTimes(1);
    });
  });
});
