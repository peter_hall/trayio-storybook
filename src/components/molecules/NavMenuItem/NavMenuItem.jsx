import React from 'react';
import PropTypes from 'prop-types';

import { hoverBorderStyleCss } from '../../particles/Borders/Borders';
import { textColourCss, hoverFillColourCss, highlightedTextColourCss } from '../../particles/Colours/Colours';
import { normalTypographyCss, activeTypographyCss } from '../../particles/Typography/Typography';

import Icon from '../../atoms/Icon/Icon';

import {
  NavListItem,
  NavListAnchor,
  PadIconSpan,
  PadContentSpan,
  ActiveChevronSpan,
} from './NavMenuItem.style';

// Have whatever react navigation library we use
// handle link clicks rather than navigating
// away from the page
const handleAnchorClick = (e, href, onNavigate) => {
  // Only prevent default event if an onNavigate
  // function has been supplied
  if (onNavigate) {
    e.preventDefault();
    onNavigate(href);
  }
};

const NavMenuItem = ({
  icon,
  href,
  isActive,
  onNavigate,
  children,
}) => (
  <NavListItem>
    <NavListAnchor
      css={[
      // Normal styles
        normalTypographyCss,
        textColourCss,

        // Is current page
        ...(isActive ? [
          activeTypographyCss,
          highlightedTextColourCss,
        ] : []),

        // Hovered
        {
          '&:hover': [
            hoverBorderStyleCss,
            hoverFillColourCss,
            highlightedTextColourCss,
          ],
        },
      ]}
      data-active={isActive}
      href={href}
      onClick={(e) => handleAnchorClick(e, href, onNavigate)}
    >
      {icon ? <PadIconSpan><Icon name={icon} /></PadIconSpan> : null}
      <PadContentSpan>{children}</PadContentSpan>
      <ActiveChevronSpan><Icon name="chevron-right" /></ActiveChevronSpan>
    </NavListAnchor>
  </NavListItem>
);

NavMenuItem.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  icon: PropTypes.string,
  href: PropTypes.string.isRequired,
  isActive: PropTypes.bool,
  onNavigate: PropTypes.func,
};

NavMenuItem.defaultProps = {
  children: null,
  icon: null,
  isActive: false,
  onNavigate: null,
};

export default NavMenuItem;
