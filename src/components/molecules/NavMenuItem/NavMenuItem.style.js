import styled from '@emotion/styled';

export const NavListItem = styled.li`
  list-style: none;
  margin: 0;
  padding: 0;
`;

export const NavListAnchor = styled.a`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: ${12 / 16}rem 1rem;
`;

// Enforce spacing between content and icons
export const PadIconSpan = styled.span`
  display: inline-block;
  margin-right: 0.5rem;
`;

// Push anything after the content to the right
export const PadContentSpan = styled.span`
  display: inline-block;
  flex: 1;
`;

// Display > only if hovered or selected
export const ActiveChevronSpan = styled.span`
  display: none;
  font-size: 0.8em;
  margin-left: 0.5rem;
  &.active, a:hover > & {
    display: inline-block;
  }
`;
