import React from 'react';
import PropTypes from 'prop-types';

import { primaryBackgroundColourCss } from '../../particles/Colours/Colours';
import MenuSpacer from '../../atoms/MenuSpacer/MenuSpacer';
import NavMenuItem from '../../molecules/NavMenuItem/NavMenuItem';

import { MainNav, NavigationUL } from './Navigation.style';

const topNavs = [
  { icon: 'home', name: 'Dashboard', route: '/dashboard' },
  { icon: 'analytics', name: 'Analytics', route: '/analytics' },
  { icon: 'book-open', name: 'Library', route: '/library' },
  { icon: 'lock-alt', name: 'Authentications', route: '/authentications' },
  { icon: 'cog', name: 'Settings & People', route: '/settings' },
];

const bottomNavs = [
  { name: 'Getting help', route: '/help' },
  { name: 'Product feedback', route: '/feedback' },
];

const Navigation = ({
  activeRoute,
  onNavigate,
}) => (
  <MainNav css={[primaryBackgroundColourCss]}>
    <NavigationUL>
      { /* Top navigation */ }
      {topNavs.map((nav) => (
        <NavMenuItem
          key={nav.route}
          icon={nav.icon}
          href={nav.route}
          isActive={activeRoute === nav.route}
          onNavigate={onNavigate}
        >
          {nav.name}
        </NavMenuItem>
      ))}
      <MenuSpacer />
      { /* Bottom navigation */ }
      <NavMenuItem
        icon="bolt"
        href="/whats-new"
        isActive={activeRoute === '/whats-new'}
        onNavigate={onNavigate}
      >
        What&apos;s new
      </NavMenuItem>
      {bottomNavs.map((nav) => (
        <NavMenuItem
          key={nav.route}
          href={nav.route}
          isActive={activeRoute === nav.route}
          onNavigate={onNavigate}
        >
          {nav.name}
        </NavMenuItem>
      ))}
    </NavigationUL>
  </MainNav>
);

Navigation.propTypes = {
  activeRoute: PropTypes.string,
  onNavigate: PropTypes.func,
};

Navigation.defaultProps = {
  activeRoute: '/dashboard',
  onNavigate: null,
};

export default Navigation;
