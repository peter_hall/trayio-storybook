import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import Navigation from './Navigation';

describe('<Navigation/>', () => {
  describe('@render', () => {
    it('should render the active nav as active', () => {
      const { queryAllByText } = render(<Navigation activeRoute="/library" />);
      const [link] = queryAllByText((_, element) => (
        element.textContent === 'Library' && element.tagName.toLowerCase() === 'a'
      ));
      expect(link).toHaveTextContent('Library'); // Got the right one?
      expect(link).toHaveAttribute('data-active', 'true');
    });
    it('should render inactive navs as inactive', () => {
      const { queryAllByText } = render(<Navigation activeRoute="/dashboard" />);
      const [link] = queryAllByText((_, element) => (
        element.textContent === 'Library' && element.tagName.toLowerCase() === 'a'
      ));
      expect(link).toHaveTextContent('Library'); // Got the right one?
      expect(link).toHaveAttribute('data-active', 'false');
    });
    it('should render the dashboard nav as active by default', () => {
      const { queryAllByText } = render(<Navigation />);
      const [link] = queryAllByText((_, element) => (
        element.textContent === 'Dashboard' && element.tagName.toLowerCase() === 'a'
      ));
      expect(link).toHaveTextContent('Dashboard'); // Got the right one?
      expect(link).toHaveAttribute('data-active', 'true');
    });
  });

  describe('@click', () => {
    it('should trigger onNavigation on the appropriate route', () => {
      const onNavigate = jest.fn();
      const { queryAllByText } = render(<Navigation onNavigate={onNavigate} />);
      const [link] = queryAllByText((_, element) => (
        element.textContent === 'Library' && element.tagName.toLowerCase() === 'a'
      ));
      expect(link).toHaveTextContent('Library'); // Got the right one?
      expect(onNavigate).toHaveBeenCalledTimes(0);
      fireEvent.click(link);
      expect(onNavigate).toHaveBeenCalledWith('/library');
      expect(onNavigate).toHaveBeenCalledTimes(1);
    });
  });
});
