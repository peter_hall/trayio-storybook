import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';

import Navigation from './Navigation';

storiesOf('Organisms/Navigation', module)
  .addDecorator(withKnobs)
  .addDecorator((story) => <div style={{ height: '100vh', display: 'flex' }}>{story()}</div>)
  .add('default', () => (
    <Navigation
      activeRoute={text('Active route', '/dashboard')}
      onNavigate={action('onNavigate')}
    />
  ));
