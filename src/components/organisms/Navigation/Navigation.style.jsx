import styled from '@emotion/styled';

export const MainNav = styled.nav`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: ${24 / 16}rem 1rem ${57 / 16}rem 1rem;
`;

export const NavigationUL = styled.ul`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin: 0;
  padding: 0;
`;
