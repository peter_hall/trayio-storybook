import styled from '@emotion/styled';

const MenuSpacerLi = styled.li`
  flex: 1;
  list-style: none;
  margin: 0;
  padding: 0;
`;

export default MenuSpacerLi;
