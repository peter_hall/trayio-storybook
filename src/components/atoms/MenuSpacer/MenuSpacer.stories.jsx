import React from 'react';
import { storiesOf } from '@storybook/react';

import MenuSpacer from './MenuSpacer';

storiesOf('Atoms/MenuSpacer', module)
  .addDecorator((story) => <div style={{ height: '100vh', display: 'flex' }}>{story()}</div>)
  .add('Horizontal', () => (
    <ul style={{ display: 'flex', flexDirection: 'row', justifyContent: 'spaceBetween' }}>
      <li>Before</li>
      <MenuSpacer />
      <li>after</li>
    </ul>
  ))
  .add('Vertical', () => (
    <ul style={{
      display: 'flex', flex: 1, flexDirection: 'column', justifyContent: 'spaceBetween',
    }}
    >
      <li>Before</li>
      <MenuSpacer />
      <li>after</li>
    </ul>
  ));
