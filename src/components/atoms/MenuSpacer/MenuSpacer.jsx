import React from 'react';

import MenuSpacerLi from './MenuSpacer.style';

const MenuSpacer = () => (
  <MenuSpacerLi aria-hidden />
);

export default MenuSpacer;
