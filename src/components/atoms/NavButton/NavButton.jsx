import React from 'react';
import PropTypes from 'prop-types';

import {
  primaryFillColourCss,
  inverseTextColourCss,
} from '../../particles/Colours/Colours';

import {
  inputTypographyCss,
} from '../../particles/Typography/Typography';

import NavButtonAnchor from './NavButton.style';

// Have whatever react navigation library we use
// handle link clicks rather than navigating
// away from the page
const handleAnchorClick = (e, href, onNavigate) => {
  // Only prevent default event if an onNavigate
  // function has been supplied
  if (onNavigate) {
    e.preventDefault();
    onNavigate(href);
  }
};

const NavButton = ({
  href,
  onNavigate,
  children,
}) => (
  <NavButtonAnchor
    css={[
      primaryFillColourCss,
      inverseTextColourCss,
      inputTypographyCss,
    ]}
    href={href}
    onClick={(e) => handleAnchorClick(e, href, onNavigate)}
  >
    {children}
  </NavButtonAnchor>
);

NavButton.propTypes = {
  href: PropTypes.string.isRequired,
  onNavigate: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

NavButton.defaultProps = {
  onNavigate: null,
  children: null,
};

export default NavButton;
