import styled from '@emotion/styled';

const NavButtonAnchor = styled.a`
  border-radius: ${4 / 16}rem; /* Integrate into Borders if any other component used it... */
  padding: ${9 / 16}rem 1rem;
  text-decoration: none; /* Todo: Integrate into Typography? */
`;

export default NavButtonAnchor;
