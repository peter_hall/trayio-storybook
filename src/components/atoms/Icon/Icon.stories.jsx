import React from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';

import Icon from './Icon';

const IconSwatch = ({ name }) => (
  <p>
    <Icon name={name} />
    {' '}
    {name}
  </p>
);
IconSwatch.propTypes = {
  name: PropTypes.string.isRequired,
};

storiesOf('Atoms/Icon', module)
  .add(
    'Solid', () => (
      <>
        <IconSwatch name="home" />
        <IconSwatch name="analytics" />
        <IconSwatch name="book-open" />
        <IconSwatch name="lock-alt" />
        <IconSwatch name="cog" />
        <IconSwatch name="bolt" />
        <IconSwatch name="globe-americas" />
        <IconSwatch name="ellipsis-h" />
        <IconSwatch name="chevron-right" />
        <IconSwatch name="exclamation-circle" />
        <IconSwatch name="mouse-pointer" />
        <IconSwatch name="clipboard-check" />
        <IconSwatch name="plug" />
        <IconSwatch name="play-circle" />
        <IconSwatch name="stop-circle" />
      </>
    ),
  );
