import styled from '@emotion/styled';

const WidthRegulatorSpan = styled.span`
  display: inline-block;
  text-align: center;
  width: 1em;
`;

export default WidthRegulatorSpan;
