import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faHome,
  faChartBar,
  faBookOpen,
  faLock,
  faCog,
  faBolt,
  faGlobeAmericas,
  faEllipsisH,
  faChevronRight,
  faExclamationCircle,
  faMousePointer,
  faClipboardCheck,
  faPlug,
  faPlayCircle,
  faStopCircle,
} from '@fortawesome/free-solid-svg-icons';

import WidthRegulatorSpan from './Icon.style';

library.add(
  faHome,
  faChartBar,
  faBookOpen,
  faLock,
  faCog,
  faBolt,
  faGlobeAmericas,
  faEllipsisH,
  faChevronRight,
  faExclamationCircle,
  faMousePointer,
  faClipboardCheck,
  faPlug,
  faPlayCircle,
  faStopCircle,
);

const renderIcon = (name) => {
  // Substitute pro icons I don't have access to
  switch (name) {
    case 'analytics': return <FontAwesomeIcon icon="chart-bar" />;
    case 'lock-alt': return <FontAwesomeIcon icon="lock" />;
    default: return <FontAwesomeIcon icon={name} />;
  }
};

const Icon = ({ name }) => (
  <WidthRegulatorSpan>
    {renderIcon(name)}
  </WidthRegulatorSpan>
);

Icon.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Icon;
